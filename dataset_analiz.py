import json
import numpy as np
import os
from PIL import Image
from torch.utils.data.dataset import Dataset

class Dataset(Dataset):

    def __init__(self, data_path, anno_path, image_format, transforms):
        self.transforms = transforms
        with open(anno_path) as fp:
            json_data = json.load(fp)
        samples = json_data['samples']
        self.classes = json_data['labels']
        self.image_format = image_format

        self.imgs = []
        self.annos = []
        self.data_path = data_path
        #print('loading', anno_path)

        for sample in samples:
            self.imgs.append(sample['image_name'])
            self.annos.append(sample['prediction'])
        for item_id in range(len(self.annos)):
            item = self.annos[item_id]
            vector = [cls in item for cls in self.classes]
            self.annos[item_id] = np.array(vector, dtype=float)

    def __getitem__(self, item):
        anno = self.annos[item]
        img_path = os.path.join(self.data_path, "image-"+str(self.imgs[item]) + "." + self.image_format)
        try:
            img = Image.open(img_path)
        except Exception as ex:
            print(img_path)
            return
        if self.transforms is not None:
            img = self.transforms(img)
        return img, anno

    def __len__(self):
        return len(self.imgs)