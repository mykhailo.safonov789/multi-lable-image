import os
import time
import numpy as np
from torch import nn
from tqdm import tqdm
from torchvision import transforms
import torch
from sklearn.metrics import precision_score, recall_score, f1_score
from torch.utils.data.dataloader import DataLoader
from matplotlib import pyplot as plt
from numpy import printoptions
import random
import json
from dataset_analiz import Dataset
from Resnext50 import Resnext50

torch.manual_seed(2020)
torch.cuda.manual_seed(2020)
np.random.seed(2020)
random.seed(2020)
torch.backends.cudnn.deterministic = True

dataset_val = Dataset("image", 'test_sample.json', "jpg", None)

#-------------------------------------------
# Initialize the training parameters.
num_workers = 8 # Number of CPU processes for data preprocessing
lr = 1e-4 # Learning rate
batch_size = 32
save_freq = 1 # Save checkpoint frequency (epochs)
test_freq = 200 # Test model frequency (iterations)
max_epoch_number = 35 # Number of epochs for training
# Note: on the small subset of data overfitting happens after 30-35 epochs

mean = [0.485, 0.456, 0.406]
std = [0.229, 0.224, 0.225]

#-------------------------------------------

def show_sample(img, binary_img_labels):
    # Convert the binary labels back to the text representation.
    img_labels = np.array(dataset_val.classes)[np.argwhere(binary_img_labels > 0)[:, 0]]
    plt.imshow(img)
    plt.title("{}".format(', '.join(img_labels)))
    plt.axis('off')
    #plt.show()

val_transform = transforms.Compose([
    transforms.Resize((256, 256)),
    transforms.ToTensor(),
    transforms.Normalize(mean, std)
])

train_transform = transforms.Compose([
    transforms.Resize((256, 256)),
    transforms.RandomHorizontalFlip(),
    transforms.ColorJitter(),
    transforms.RandomAffine(degrees=20, translate=(0.2, 0.2), scale=(0.5, 1.5),
                            shear=None, resample=False,
                            fillcolor=False),
    transforms.ToTensor(),
    transforms.Normalize(mean, std)
])

test_annotations = "test_sample.json"
train_annotations = "train_sample.json"

test_dataset = Dataset("image", test_annotations, "jpg", val_transform)
train_dataset = Dataset("image", train_annotations, "jpg", train_transform)

train_dataloader = DataLoader(train_dataset, batch_size=batch_size, num_workers=num_workers, shuffle=True,
                              drop_last=True)
test_dataloader = DataLoader(test_dataset, batch_size=batch_size, num_workers=num_workers)

num_train_batches = int(np.ceil(len(train_dataset) / batch_size))

def main():
#    for sample_id in range(5000):
#            dataset_val.__getitem__(sample_id)
    model = Resnext50(len(train_dataset.classes))
    model.train()

    optimizer = torch.optim.Adam(model.parameters(), lr=lr)

    # Loss function
    criterion = nn.BCELoss()

    epoch = 0
    iteration = 0
    while True:
        batch_losses = []
        for imgs, targets in train_dataloader:

            optimizer.zero_grad()

            model_result = model(imgs)
            loss = criterion(model_result, targets.type(torch.float))

            batch_loss_value = loss.item()
            loss.backward()
            optimizer.step()

            batch_losses.append(batch_loss_value)

            if iteration % test_freq == 0:
                model.eval()
                with torch.no_grad():
                    model_result = []
                    targets = []
                    for imgs, batch_targets in test_dataloader:
                        model_batch_result = model(imgs)
                        model_result.extend(model_batch_result.cpu().numpy())
                        targets.extend(batch_targets.cpu().numpy())


                print("epoch:{:2d} iter:{:3d} test: "
                      "micro f1: {:.3f} "
                      "macro f1: {:.3f} "
                      "samples f1: {:.3f}".format(epoch, iteration,))

                model.train()
            iteration += 1

        loss_value = np.mean(batch_losses)
        print("epoch:{:2d} iter:{:3d} train: loss:{:.3f}".format(epoch, iteration, loss_value))

        epoch += 1
        if max_epoch_number < epoch:
            break

if __name__ == "__main__":
    main()